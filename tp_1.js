let studentListe = [
  {
    firstName: 'Elisabeth',
    lastName: 'Nathanael',
    matters: [
      {
        matterName: 'Français',
        values: [14, 19, 6, 6, 5, 8, 4]
      },
      {
        matterName: 'Mathémathiques',
        values: [15, 13, 9, 19, 6]
      },
      { matterName: 'Géographie', values: [12, 3, 17, 14, 6] },
      { matterName: 'Histoire', values: [18, 2, 8, 18, 19, 3] },
      { matterName: 'Physique', values: [9, 6, 11, 13, 11, 1] },
      { matterName: 'Chimie', values: [14, 14, 1, 11, 13] },
      {
        matterName: 'Anglais',
        values: [2, 0, 0, 0, 6, 6, 4, 14]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          19, 3, 4, 8,
          18, 10, 11
        ]
      },
      { matterName: 'Sport', values: [7, 14, 0, 9, 13, 18] }
    ]
  },
  {
    firstName: 'Frederic',
    lastName: 'Alix',
    matters: [
      {
        matterName: 'Français',
        values: [
          2, 16, 9, 14,
          2, 15, 17
        ]
      },
      { matterName: 'Mathémathiques', values: [18, 8, 16, 18, 4] },
      { matterName: 'Géographie', values: [2, 10, 11, 18, 10] },
      { matterName: 'Histoire', values: [7, 14, 16, 15, 14, 8] },
      { matterName: 'Physique', values: [18, 12, 5, 12, 9, 18] },
      { matterName: 'Chimie', values: [7, 8, 6, 17, 13] },
      {
        matterName: 'Anglais',
        values: [
          2, 7, 9, 12,
          4, 1, 9, 12
        ]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          8, 2, 15, 3,
          13, 11, 16
        ]
      },
      { matterName: 'Sport', values: [15, 1, 18, 18, 7, 19] }
    ]
  },
  {
    firstName: 'Antoine',
    lastName: 'Cambon',
    matters: [
      {
        matterName: 'Français',
        values: [
          6, 19, 0, 6,
          10, 5, 0
        ]
      },
      { matterName: 'Mathémathiques', values: [2, 2, 3, 19, 2] },
      { matterName: 'Géographie', values: [7, 14, 16, 16, 11] },
      { matterName: 'Histoire', values: [6, 13, 8, 16, 6, 15] },
      { matterName: 'Physique', values: [8, 16, 9, 7, 1, 2] },
      { matterName: 'Chimie', values: [9, 6, 12, 18, 15] },
      {
        matterName: 'Anglais',
        values: [
          15, 3, 16, 10,
          10, 5, 11, 6
        ]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          18, 10, 14, 8,
          3, 3, 1
        ]
      },
      { matterName: 'Sport', values: [4, 1, 16, 9, 7, 17] }
    ]
  },
  {
    firstName: 'Mickael',
    lastName: 'Favres',
    matters: [
      {
        matterName: 'Français',
        values: [
          7, 18, 8, 6,
          6, 15, 2
        ]
      },
      { matterName: 'Mathémathiques', values: [3, 2, 12, 0, 14] },
      { matterName: 'Géographie', values: [14, 1, 13, 6, 16] },
      { matterName: 'Histoire', values: [7, 17, 5, 15, 10, 16] },
      { matterName: 'Physique', values: [15, 0, 12, 16, 3, 5] },
      { matterName: 'Chimie', values: [17, 11, 2, 9, 9] },
      {
        matterName: 'Anglais',
        values: [
          12, 15, 19, 1,
          11, 13, 19, 19
        ]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          16, 6, 16, 14,
          17, 17, 0
        ]
      },
      { matterName: 'Sport', values: [11, 6, 15, 18, 5, 7] }
    ]
  },
  {
    firstName: 'Nicolas',
    lastName: 'Levaufre',
    matters: [
      {
        matterName: 'Français',
        values: [
          9, 10, 1, 13,
          19, 15, 10
        ]
      },
      { matterName: 'Mathémathiques', values: [13, 3, 2, 17, 16] },
      { matterName: 'Géographie', values: [6, 0, 1, 16, 8] },
      { matterName: 'Histoire', values: [14, 10, 10, 19, 11, 1] },
      { matterName: 'Physique', values: [5, 10, 12, 17, 14, 6] },
      { matterName: 'Chimie', values: [6, 19, 0, 11, 2] },
      {
        matterName: 'Anglais',
        values: [
          14, 1, 19, 11,
          17, 6, 7, 7
        ]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          12, 6, 4, 8,
          6, 6, 7
        ]
      },
      { matterName: 'Sport', values: [7, 17, 7, 14, 17, 2] }
    ]
  },
  {
    firstName: 'Amélia',
    lastName: 'Arnoult',
    matters: [
      {
        matterName: 'Français',
        values: [
          18, 17, 13, 5,
          2, 2, 14
        ]
      },
      { matterName: 'Mathémathiques', values: [2, 16, 10, 12, 11] },
      { matterName: 'Géographie', values: [9, 14, 10, 14, 1] },
      { matterName: 'Histoire', values: [19, 19, 18, 7, 0, 7] },
      { matterName: 'Physique', values: [9, 15, 1, 12, 4, 16] },
      { matterName: 'Chimie', values: [12, 11, 18, 17, 13] },
      {
        matterName: 'Anglais',
        values: [
          8, 9, 3, 11,
          12, 10, 15, 9
        ]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          9, 19, 15, 17,
          1, 19, 19
        ]
      },
      { matterName: 'Sport', values: [0, 7, 13, 12, 6, 6] }
    ]
  },
  {
    firstName: 'Thomas',
    lastName: 'Clef',
    matters: [
      {
        matterName: 'Français',
        values: [
          7, 16, 11, 5,
          14, 18, 1
        ]
      },
      { matterName: 'Mathémathiques', values: [6, 18, 0, 19, 19] },
      { matterName: 'Géographie', values: [15, 15, 7, 19, 13] },
      { matterName: 'Histoire', values: [14, 2, 11, 2, 10, 5] },
      { matterName: 'Physique', values: [6, 1, 9, 4, 12, 15] },
      { matterName: 'Chimie', values: [1, 7, 12, 16, 3] },
      {
        matterName: 'Anglais',
        values: [
          15, 19, 8, 11,
          15, 1, 11, 0
        ]
      },
      {
        matterName: 'Science et Vie de la Terre',
        values: [
          13, 9, 14, 8,
          9, 7, 1
        ]
      },
      { matterName: 'Sport', values: [3, 9, 4, 10, 3, 8] }
    ]
  }
]

// FONCTION DE CALCUL

/**
 * fonction 1.A :
 * Calculer la moyenne de chaque matière d'un élève, et la retourner sous forme de dictionnaire
 * 
 * @param {Object} eleve Objet de l'élève
 * 
 * @returns {Object} Objet avec les moyennes par matière
 */
function calculMoyenneEleve(eleve) {
  let resultat = {}

  for(let i = 0; i < eleve.matters.length; i++) {
    resultat[eleve.matters[i].matterName] = moyenne(eleve.matters[i].values)
  }

  return resultat
}

/**
 * Calculer une moyenne à partir d'une liste de chiffre
 * 
 * @param {Array} listeNote Liste des notes
 * @returns {Float} Moyenne de la liste des notes
 */
function moyenne(listeNote) {
  let moyenne = 0

  for(let i = 0; i < listeNote.length; i++) {
    moyenne += listeNote[i]
  }

  return moyenne / listeNote.length
}

//console.log(calculMoyenneEleve(studentListe[0]))

/**
 * Fonction 1.B :
 * Calculer la moyenne générale d'un élève et la retourner sous un chiffre
 * 
 * @param {Object} eleve Objet de l'élève
 * 
 * @returns {Float} Moyenne générale de l'élève
 */
function calculMoyenneGeneraleEleve(eleve) {
  let moyenneMatiere = calculMoyenneEleve(eleve)
  let valuesMoyenne = Object.values(moyenneMatiere)

  return moyenne(valuesMoyenne)
}

//console.log(calculMoyenneGeneraleEleve(studentListe[0]))

/**
 * Fonction 1.C :
 * Calculer la moyenne de chaques matières de tous les élèves, sous forme de dictionnaire
 * 
 * @param {Array} listeEleve Liste des élèves
 * 
 * @returns {Object} Objet avec les moyennes par matière pour chaque élève
 */
function calculMoyenneListeEleve(listeEleve) {
  let resultat = {}

  for(let i = 0; i < listeEleve.length; i++) {
    resultat[listeEleve[i].lastName] = calculMoyenneEleve(listeEleve[i])
  }

  return resultat
}

//console.log(calculMoyenneListeEleve(studentListe))

/**
 * Fonction 1.D :
 * Calculer la moyenne générale de tous les élèves sous forme de dictionnaire
 * 
 * @param {Array} listeEleve Liste des élèves
 * 
 * @returns {Object} Objet avec les moyennes générale de chaque élève
 */
function calculMoyenneGeneraleListeEleve(listeEleve) {
  let resultat = {}

  for(let i = 0; i < listeEleve.length; i++) {
    resultat[listeEleve[i].lastName] = calculMoyenneGeneraleEleve(listeEleve[i])
  }

  return resultat
}

//console.log(calculMoyenneGeneraleListeEleve(studentListe))

// FONCTION D'AFFICHAGE

/**
 * Fonction 2.A :
 * Afficher toutes les notes de chaque matières d'un élève
 * 
 * @param {Object} eleve Objet de l'élève
 */
function afficherNoteEleve(eleve) {
  console.log("Notes par matière de " + eleve.firstName + " " + eleve.lastName)

  for(let i = 0; i < eleve.matters.length; i++) {
    console.log(eleve.matters[i].matterName + " : " + eleve.matters[i].values.join(" "))
  }
}

//afficherNoteEleve(studentListe[0])

/**
 * Fonction 2.B :
 * Afficher les moyennes de chaque matière et la moyenne générale d'un élève.
 * 
 * @param {Object} eleve Objet de l'élève
 */
function afficherMoyenneEleve(eleve) {
  console.log("Moyenne générale et par matière de " + eleve.firstName + " " + eleve.lastName)

  let moyenneParMatiere = calculMoyenneEleve(eleve)
  
  for(x in moyenneParMatiere) {
    console.log(x + " : " + moyenneParMatiere[x])
  }

  console.log("Moyenne générale : " + calculMoyenneGeneraleEleve(eleve))
}

//afficherMoyenneEleve(studentListe[0])

/**
 * Fonction 2.C :
 * Afficher les moyennes générales de tous les élèves
 * 
 * @param {Array} listeEleve Liste de tous les élèves
 */
function afficherMoyenneGeneraleListeEleve(listeEleve) {
  for(let i = 0; i < listeEleve.length; i++) {
    afficherMoyenneGeneraleEleve(listeEleve[i])
  }
}

/**
 * Afficher la moyenne générale d'un élève
 * 
 * @param {Object} eleve Objet de l'élève
 */
function afficherMoyenneGeneraleEleve(eleve) {
  console.log("Moyenne générale de " + eleve.firstName + " " + eleve.lastName + " : " + calculMoyenneGeneraleEleve(eleve))
}

//afficherMoyenneGeneraleListeEleve(studentListe)

/**
 * Fonction 2.D :
 * Afficher si les moyennes des étudiants sont au dessus ou en dessous de la moyenne de la classe dans la matière
 * 
 * @param {Array} listeEleve Liste de tous les élèves
 */
function afficherMoyenneListeEleveApprox(listeEleve) {
  let moyenneGeneraleEleves = calculMoyenneListeEleve(listeEleve)

  for(x in moyenneGeneraleEleves) {
    console.log("Moyenne de " + x + " :")
    for(y in moyenneGeneraleEleves[x]) {
      console.log("moyenne " + y, moyenneGeneraleEleves[x][y] >= moyenneParMatiere(y, listeEleve) ? "est supérieur à la moyenne" : "est inférieur à la moyenne")
    }
    console.log(" ")
  }
}

/**
 * Calcul la moyenne de la classe dans une matière
 * 
 * @param {String} matter Matière de la moyenne
 * @param {Array} listeEleve Liste de tous les élèves
 * 
 * @returns {Float} Moyenne de la classe dans la matière
 */
function moyenneParMatiere(matter, listeEleve) {
  let listeMoyenne = []

  for(let i = 0; i < listeEleve.length; i++) {
    listeMoyenne.push(calculMoyenneEleve(listeEleve[i])[matter])
  }

  return moyenne(listeMoyenne)
}

//afficherMoyenneListeEleveApprox(studentListe)
