/**
 * Retourne un chiffre aléatoire entre min et max
 * 
 * @param {Integer} min Valeur minimum
 * @param {Integer} max Valeur Maximum
 * @returns 
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

/**
 * Retourne une liste de chiffre aléatoire selon la matière
 * 
 * @param {String} matter Matière
 * 
 * @returns {Array} Liste de note aléatoire
 */
function getRandonArrayByMatter(matter) {
    let array = []

    for(let i = 0; i < matter.length; i++) {
        array.push(getRandomInt(0, 20))
    }

    return array
}

const listStudentName = [
    {
        "firstName": "Nathanael",
        "lastName": "Elisabeth"
    },
    {
        "firstName": "Frédéric",
        "lastName": "Alix"
    }
]

const subjectList = [
    "Mathématiques",
    "Histoire",
    "Français"
]

/**
 * Retourne une liste d'élève avec des notes aléatoire
 * 
 * @param {Array} eleveListe Liste des nom/prénom des élèves
 * @param {Array} matterListe Liste des matières
 * 
 * @returns {Array} Liste d'objet d'élève
 */
function genererListeEleve(eleveListe, matterListe) {
    let resultat = []

    for(let i = 0; i < eleveListe.length; i++) {
        for(let j = 0; j < matterListe.length; j++) {
            eleveListe[i][matterListe[j]] = getRandonArrayByMatter(matterListe[j])
        }
        resultat.push(eleveListe[i])
    }

    return resultat
}

console.log(genererListeEleve(listStudentName, subjectList))